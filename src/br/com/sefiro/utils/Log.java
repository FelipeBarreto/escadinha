package br.com.sefiro.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {

    private static final Logger info = Logger.getLogger("INFO");
    private static final Logger debug = Logger.getLogger("DEBUG");
    private static final Logger error = Logger.getLogger("ERROR");
    private static final Logger warning = Logger.getLogger("WARNING");

    public static void info(String log){
        info.log(Level.INFO, log);
    }

    public static void debug(String log){
        info.log(Level.ALL, log);
    }

    public static void error(String log){
        info.log(Level.SEVERE, log);
    }

    public static void warning(String log){
        info.log(Level.WARNING, log);
    }
}
