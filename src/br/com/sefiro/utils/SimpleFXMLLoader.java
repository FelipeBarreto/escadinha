package br.com.sefiro.utils;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.File;
import java.io.IOException;

public class SimpleFXMLLoader {

    public Parent getEntry(FXMLEntry entry) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(entry.getOrigin().getResource(entry.getPath()));
        return loader.load();
    }
}
