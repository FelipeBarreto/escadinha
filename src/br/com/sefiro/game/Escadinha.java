package br.com.sefiro.game;

import br.com.sefiro.utils.FXMLEntry;
import br.com.sefiro.utils.Log;
import br.com.sefiro.utils.SimpleFXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Escadinha {

    public Parent escadinhaUI;

    public void start(Stage primaryStage){
        initComponents();
        Scene scene = new Scene(escadinhaUI);
        primaryStage.setScene(scene);

        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    private void initComponents() {
        SimpleFXMLLoader loader = new SimpleFXMLLoader();
        try {
            escadinhaUI = loader.getEntry(FXMLEntry.ESCADINHA);
        } catch (IOException e) {
            Log.error("Error loading FXML files");
        }
    }
}
