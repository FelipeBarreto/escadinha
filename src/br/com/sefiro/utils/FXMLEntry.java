package br.com.sefiro.utils;

import br.com.sefiro.components.escadinha.EscadinhaController;

public enum FXMLEntry {

    ESCADINHA (EscadinhaController.class, "escadinha.fxml");

    Class origin;
    String path;

    FXMLEntry(Class origin, String path){
        this.origin = origin;
        this.path = path;
    }

    public Class getOrigin() {
        return origin;
    }

    public String getPath(){
        return path;
    }
}
