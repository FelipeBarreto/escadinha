package br.com.sefiro.bootstrap;

import br.com.sefiro.game.Escadinha;
import br.com.sefiro.settings.Settings;
import javafx.application.Application;
import javafx.stage.Stage;

public class Launcher extends Application{

    public static void main(String[] args){
        Settings.load();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Escadinha game = new Escadinha();
        game.start(primaryStage);
    }
}
